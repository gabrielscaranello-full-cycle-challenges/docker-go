# Full Cycle Course - Docker GoLang Challenge

This repository is part of a series of codes created to meet the challenges of the Full Cycle 3.0 course

## Run from Dockerhub

[Dockerhub Repository](https://hub.docker.com/r/gabrielscaranello/full-cycle-challenges-docker-go-lang)

```bash
docker run gabrielscaranello/full-cycle-challenges-docker-go-lang
```

## Run from Dockerfile

```bash
docker build -t full-cycle-challenges-docker-go-lang .
docker run full-cycle-challenges-docker-go-lang
```
