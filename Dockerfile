FROM golang:1.21.5-alpine as builder

WORKDIR /usr/src/app

COPY main.go .

RUN go build -o ./full-cycle ./main.go

FROM scratch

WORKDIR /

COPY --from=builder /usr/src/app/full-cycle .

ENTRYPOINT [ "./full-cycle" ]

